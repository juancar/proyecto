<?php namespace Alterar;

interface Alterar{

	function alta($objeto);
	function baja($objeto);
	function modificar($objeto);
}

 ?>
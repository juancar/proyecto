-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2018 at 12:19 PM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `articulo`
--
CREATE DATABASE IF NOT EXISTS `articulo` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `articulo`;

-- --------------------------------------------------------

--
-- Table structure for table `referencias`
--

CREATE TABLE `referencias` (
  `id` int(11) NOT NULL,
  `referencia` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `referencias`
--

INSERT INTO `referencias` (`id`, `referencia`, `descripcion`, `precio`) VALUES
(2, 'asc234', 'Goma', 2.5),
(4, 'CDW', 'Mochila', 20),
(12, 'nb', 'nb', 6),
(20, 'weer', 'pinzas', 5),
(22, 'asc23', 'Goma', 2.5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `referencias`
--
ALTER TABLE `referencias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `referencias`
--
ALTER TABLE `referencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;--
-- Database: `ong`
--
CREATE DATABASE IF NOT EXISTS `ong` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ong`;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(1, 'Informática'),
(3, 'Libros'),
(4, 'Música'),
(2, 'Ropa');

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`id`, `nombre`) VALUES
(2, 'Aceptado'),
(1, 'Propuesto'),
(4, 'Retirado'),
(3, 'Sin existencias');

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `descripcion` text CHARACTER SET latin1 NOT NULL,
  `ruta_imagen` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evento`
--

INSERT INTO `evento` (`id`, `nombre`, `descripcion`, `ruta_imagen`) VALUES
(1, 'Concierto', 'Concierto de rock', 'img/evento/concierto.jpg'),
(2, 'Teatro', 'Taller de teatro', 'img/evento/teatro.jpg'),
(3, 'Carrera', 'Carrera solidaria', 'img/evento/carrera.jpg'),
(4, 'Senderismo', 'Senderismo en Ribera Sacra', 'img/evento/senderismo.jpg'),
(11, 'Cine', 'Cine para todos', 'img/evento/cine.jpg'),
(16, 'Meditación', 'Conferencia sobre meditación', 'img/evento/conferencia.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `evento_lugar`
--

CREATE TABLE `evento_lugar` (
  `id_evento_lugar` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `id_lugar` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `aforo` int(11) NOT NULL,
  `precio_entrada` float NOT NULL,
  `entradas_disponibles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evento_lugar`
--

INSERT INTO `evento_lugar` (`id_evento_lugar`, `id_evento`, `id_lugar`, `fecha`, `aforo`, `precio_entrada`, `entradas_disponibles`) VALUES
(1, 1, 4, '2018-07-25', 6, 6, 1),
(2, 2, 3, '2018-10-31', 35, 3, 34),
(3, 3, 4, '2018-05-28', 50, 69, 50),
(4, 3, 3, '2018-08-31', 2, 48, 1),
(5, 4, 1, '2018-07-27', 30, 3, 30),
(9, 2, 4, '2018-05-17', 5, 5, 5),
(11, 2, 3, '2018-05-30', 53, 3, 5),
(12, 1, 4, '2018-05-17', 6, 69, 6),
(14, 11, 3, NULL, 30, 5, 30),
(16, 16, 2, NULL, 50, 20, 50);

-- --------------------------------------------------------

--
-- Table structure for table `evento_participante`
--

CREATE TABLE `evento_participante` (
  `id_evento` int(11) NOT NULL,
  `id_participante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evento_participante`
--

INSERT INTO `evento_participante` (`id_evento`, `id_participante`) VALUES
(1, 4),
(1, 5),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(12, 9),
(12, 11),
(14, 9),
(16, 2),
(16, 6),
(16, 8),
(16, 15);

-- --------------------------------------------------------

--
-- Table structure for table `fechas_posibles`
--

CREATE TABLE `fechas_posibles` (
  `id_evento` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fechas_posibles`
--

INSERT INTO `fechas_posibles` (`id_evento`, `fecha`) VALUES
(1, '2018-01-31'),
(1, '2018-05-10'),
(1, '2018-05-14'),
(1, '2018-05-16'),
(1, '2018-05-17'),
(1, '2018-05-18'),
(1, '2018-05-22'),
(1, '2018-05-23'),
(1, '2018-05-31'),
(1, '2018-07-19'),
(1, '2018-07-27'),
(1, '2018-08-03'),
(1, '2018-08-04'),
(1, '2018-09-27'),
(1, '2018-12-31'),
(1, '2019-01-01'),
(1, '2019-01-30'),
(1, '2019-01-31'),
(1, '2019-02-22'),
(2, '2018-05-14'),
(2, '2018-05-17'),
(2, '2018-05-28'),
(2, '2018-05-31'),
(2, '2018-06-29'),
(2, '2018-08-02'),
(2, '2018-08-03'),
(2, '2018-09-05'),
(3, '2018-05-17'),
(3, '2018-07-13'),
(3, '2018-08-03'),
(3, '2018-08-04'),
(3, '2018-10-13'),
(4, '2018-05-17'),
(4, '2018-05-26'),
(4, '2018-05-31'),
(4, '2018-07-27'),
(4, '2018-07-31'),
(4, '2018-12-12'),
(4, '2018-12-24'),
(5, '2018-05-17'),
(5, '2018-05-18'),
(5, '2018-05-25'),
(5, '2018-05-26'),
(5, '2018-05-30'),
(5, '2018-07-27'),
(5, '2018-11-13'),
(9, '2018-05-17'),
(9, '2018-05-18'),
(11, '2018-05-17'),
(11, '2018-05-30'),
(12, '2018-05-17'),
(12, '2018-05-30'),
(14, '2018-07-28'),
(14, '2019-02-12'),
(16, '2019-01-23'),
(16, '2019-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `CIF` varchar(9) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `domicilio_fiscal` varchar(45) CHARACTER SET latin1 NOT NULL,
  `web` varchar(60) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`id`, `CIF`, `nombre`, `domicilio_fiscal`, `web`) VALUES
(1, '777777Q', 'Metallica', 'c/ Sanjurjo nº 2 , Vigo Pontevedra', 'www.metallica.com'),
(2, '34443D', 'Anima', 'C Aeropuerto', 'www.anima.es');

-- --------------------------------------------------------

--
-- Table structure for table `imagenes_prod`
--

CREATE TABLE `imagenes_prod` (
  `id` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `ruta` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imagenes_prod`
--

INSERT INTO `imagenes_prod` (`id`, `id_prod`, `ruta`) VALUES
(1, 1, 'img/bd/camisa1/camisa1a.jpg'),
(2, 2, 'img/bd/portatil1/portatil1a.jpg'),
(3, 3, 'img/bd/bestiario/bestiario1.jpg'),
(4, 4, 'img/bd/revolver/discoa.jpg'),
(7, 1, 'img/bd/camisa1/camisa1b.jpg'),
(8, 2, 'img/bd/portatil1/portatil1b.jpg'),
(11, 3, 'img/bd/bestiario/bestiario2.jpg'),
(12, 4, 'img/bd/revolver/discob.jpg'),
(13, 5, 'img/bd/pantalon1/pantalon1a.jpg'),
(14, 5, 'img/bd/pantalon1/pantalon1b.jpg'),
(18, 64, 'img/bd/black/disco1.jpg'),
(19, 64, 'img/bd/black/disco2.jpg'),
(20, 65, 'img/bd/movil1/movil1a.jpg'),
(21, 65, 'img/bd/movil1/movil1b.jpg'),
(24, 66, 'img/bd/movil2/movil2a.jpg'),
(25, 66, 'img/bd/movil2/movil2b.jpg'),
(28, 67, 'img/bd/artaud/artaud1.jpg'),
(29, 67, 'img/bd/artaud/artaud2.jpg'),
(30, 68, 'img/bd/clash/disco1.jpg'),
(31, 68, 'img/bd/clash/disco2.jpg'),
(32, 69, 'img/bd/pijama/pijama1.jpg'),
(33, 69, 'img/bd/pijama/pijama2.jpg'),
(34, 70, 'img/bd/ficciones/ficciones1.jpg'),
(35, 70, 'img/bd/ficciones/ficciones2.jpg'),
(36, 71, 'img/bd/ruido/ruido1.jpg'),
(37, 71, 'img/bd/ruido/ruido2.jpg'),
(38, 72, 'img/bd/portatil2/portatil2a.jpg'),
(39, 72, 'img/bd/portatil2/portatil2b.jpg'),
(40, 73, 'img/bd/sudadera/sudadera1a.jpg'),
(41, 73, 'img/bd/sudadera/sudadera1b.jpg'),
(44, 74, 'img/bd/strange/disco1.jpg'),
(45, 74, 'img/bd/strange/disco2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `lugar`
--

CREATE TABLE `lugar` (
  `id` int(11) NOT NULL,
  `lugar` varchar(60) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lugar`
--

INSERT INTO `lugar` (`id`, `lugar`) VALUES
(4, 'Coruña'),
(3, 'Ourense'),
(2, 'Pontevedra'),
(1, 'Vigo');

-- --------------------------------------------------------

--
-- Table structure for table `participante`
--

CREATE TABLE `participante` (
  `id` int(11) NOT NULL,
  `NIF` varchar(9) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `localidad` varchar(45) CHARACTER SET latin1 NOT NULL,
  `provincia` varchar(45) CHARACTER SET latin1 NOT NULL,
  `grupo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participante`
--

INSERT INTO `participante` (`id`, `NIF`, `nombre`, `apellido1`, `apellido2`, `email`, `telefono`, `direccion`, `localidad`, `provincia`, `grupo`) VALUES
(1, '5555555F', 'Pedro', 'Pérez', 'Pérez', 'pedro@pedro.com', '44444444', 'c/ Rosalia de Castro nº 4', 'Vigo', 'Pontevedra', NULL),
(2, '7777777L', 'Miguel', 'García', 'García', 'miguel@miguel.com', '55666666', 'C/ Carballeira nº 8', 'Santiago', 'Coruña', 1),
(3, '88888G', 'Juan', 'López', 'López', 'juan@lopez.com', '7897655', 'rua carballeira nº 2', 'Santiago', 'Coruña', 1),
(4, '99999Q', 'Francisco', 'Milanes', 'Milanes', 'francisco@milanes.com', '4444444', 'av colombia 1', 'Vigo', 'Pontevedra', 1),
(5, '34543433', 'Paco', 'Lucia', 'Lucia', 'paco@gmail.com', '666578899', 'Av castelao nº 5', 'Santiago', 'Coruña', 1),
(6, '45454D', 'Andrés', 'Ramírez', 'Ramírez', 'andres@abc.com', '6767677', 'Av Madrid 5', 'Vigo', 'Pontevedra', 1),
(7, '87676Q', 'José', 'Blanco', 'Blanco', 'jose@abc.com', '657575', 'Av García n 5', 'Pontevedra', 'Pontevedra', 2),
(8, '348484Y', 'Ana', 'Ferreiro', 'Ferreiro', 'ana@abc.com', '5675756', 'Calle Zaragoza n 3', 'Coruña', 'Coruña', 2),
(9, '567897A', 'Mario', 'Bros', 'Bros', 'mario@abc.com', '5454567', 'C/ Ramiro n 8', 'Vigo', 'Vigo', NULL),
(10, '468695T', 'Carlos', 'Rivas', 'Rivas', 'carlos@abc.com', '5678666', 'C/ bienvenido 8', 'Santiago', 'Coruña', NULL),
(11, '89898D', 'Oscar', 'Vil', 'Vil', 'oscar@abc.com', '6766767', 'C/ Santiago n 5', 'Lugo', 'Lugo', NULL),
(12, '78787Y', 'Santiago', 'Berta', 'Berta', 'santiago@abc.com', '8777676', 'Av Galicia n 8', 'Vigo', 'Pontevedra', NULL),
(13, '454454W', 'Rodrigo', 'Rodríguez', 'Rodríguez', 'rodrigo@abc.com', '5656565', 'C/ Aragon n 7', 'Coruña', 'Coruña', NULL),
(14, '76767R', 'Laura', 'Arnoso', 'Arnoso', 'laura@abc.com', '656565', 'c/ Lorenzo n 2', 'Santiago', 'Santiago', NULL),
(15, '434343W', 'Tomas', 'Rivera', 'Rivera', 'tomas@abc.com', '445454', 'C/ Aquino n 3', 'Pontevedra', 'Pontevedra', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `descripcion` text CHARACTER SET latin1 NOT NULL,
  `categoria` int(11) NOT NULL,
  `fecha_fin_campania` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `categoria`, `fecha_fin_campania`) VALUES
(1, 'Camisa', 'Camisa de cuadros', 2, '2019-12-30'),
(2, 'Portátil', 'Portátil HP última generación', 1, '2018-06-28'),
(3, 'Libro Bestiario', 'Libro de cuentos', 1, '2018-06-27'),
(4, 'Revolver', 'Disco del grupo The Beatles', 1, '2018-06-26'),
(5, 'Pantalón', 'Pantalón marrón', 2, '2018-10-31'),
(64, 'Black Sabbath', 'Disco grupo Black Sabbath', 4, '2018-12-11'),
(65, 'Móvil', 'Móvil Samsung color negro', 1, '2018-07-30'),
(66, 'Móvil', 'Móvil Samsung color blanco', 1, '2019-03-26'),
(67, 'Libro de Artaud', 'Libro Carta a los poderes', 3, '2018-10-30'),
(68, 'London Calling', 'Disco del grupo The Clash', 4, '2019-02-26'),
(69, 'Pijama', 'Pijama de algodón', 2, '2018-07-31'),
(70, 'Ficciones', 'Libro de Jorge Luis Borges', 3, '2018-09-26'),
(71, 'El ruido y la furia', 'Libro de William Faulkner', 3, '2018-12-29'),
(72, 'Portátil', 'Portátil con ratón', 1, '2019-01-29'),
(73, 'Sudadera', 'Sudadera de algodón', 2, '2018-11-22'),
(74, 'Strange Days', 'Disco del grupo The Doors', 4, '2018-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `producto_tienda`
--

CREATE TABLE `producto_tienda` (
  `id_producto_tienda` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `id_tienda` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` float NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `producto_tienda`
--

INSERT INTO `producto_tienda` (`id_producto_tienda`, `id_prod`, `id_tienda`, `stock`, `precio`, `estado`) VALUES
(1, 1, 3, 3, 1, 4),
(2, 1, 2, 4, 26, 2),
(3, 1, 3, 40, 3, 2),
(4, 2, 2, 70, 500, 2),
(9, 2, 1, 3, 5, 2),
(10, 2, 3, 12, 600, 2),
(11, 3, 2, 12, 10, 1),
(12, 3, 2, 23, 9, 2),
(13, 3, 3, 31, 8, 2),
(14, 4, 3, 4, 5, 3),
(15, 4, 2, 7, 13, 4),
(16, 4, 3, 7, 10.5, 2),
(17, 5, 2, 3, 2, 2),
(36, 64, 1, 20, 30, 2),
(37, 64, 2, 34, 35, 4),
(38, 64, 3, 0, 34, 3),
(39, 65, 1, 200, 200, 4),
(40, 65, 2, 98, 210, 4),
(41, 65, 3, 56, 215, 4),
(42, 66, 1, 87, 198, 2),
(43, 66, 2, 76, 184, 2),
(44, 66, 3, 0, 182, 3),
(45, 67, 1, 56, 12, 2),
(46, 67, 2, 300, 14, 2),
(47, 67, 3, 67, 13, 2),
(48, 68, 1, 78, 32, 1),
(49, 68, 2, 32, 34, 1),
(50, 68, 3, 45, 0, 1),
(55, 68, 1, 53, 34, 2),
(56, 1, 2, 45, 0, 1),
(57, 68, 2, 67, 21, 2),
(58, 68, 3, 0, 21, 3),
(59, 69, 1, 99, 20, 2),
(60, 69, 2, 0, 21, 3),
(61, 69, 3, 45, 21, 4),
(62, 70, 1, 56, 13, 2),
(63, 70, 2, 45, 11, 2),
(64, 70, 3, 32, 10, 2),
(65, 71, 1, 45, 24, 2),
(66, 71, 2, 56, 20, 2),
(67, 71, 3, 34, 18, 2),
(68, 72, 1, 67, 800, 2),
(69, 72, 2, 0, 780, 3),
(70, 72, 3, 25, 770, 3),
(71, 73, 1, 21, 20, 1),
(72, 73, 2, 45, 21, 2),
(73, 73, 3, 20, 20, 4),
(74, 74, 1, 56, 21, 2),
(75, 74, 2, 32, 20, 2),
(76, 74, 3, 0, 22, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reserva_event`
--

CREATE TABLE `reserva_event` (
  `id_reserva` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reserva_event`
--

INSERT INTO `reserva_event` (`id_reserva`, `id_usuario`, `id_evento`, `cantidad`, `precio_total`, `fecha`) VALUES
(3, 15, 1, 12, 20, '2018-05-16'),
(6, 15, 1, 1, 66, '2018-05-27'),
(7, 15, 1, 1, 66, '2018-05-27'),
(8, 19, 1, 1, 6, '2018-05-27'),
(9, 19, 2, 1, 3, '2018-05-27'),
(10, 19, 3, 1, 48888, '2018-05-27'),
(11, 19, 4, 1, 3, '2018-05-27');

-- --------------------------------------------------------

--
-- Table structure for table `reserva_prod`
--

CREATE TABLE `reserva_prod` (
  `id_reserva` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_tienda` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reserva_prod`
--

INSERT INTO `reserva_prod` (`id_reserva`, `id_usuario`, `id_producto`, `id_tienda`, `cantidad`, `precio_total`, `fecha`) VALUES
(1, 15, 68, 1, 1, 34, '2018-05-27'),
(2, 19, 68, 1, 1, 34, '2018-05-27'),
(3, 19, 1, 2, 1, 26, '2018-05-27'),
(4, 19, 4, 3, 1, 10.5, '2018-05-27'),
(5, 19, 69, 1, 1, 20, '2018-05-27'),
(6, 19, 68, 1, 1, 34, '2018-05-29');

-- --------------------------------------------------------

--
-- Table structure for table `tienda`
--

CREATE TABLE `tienda` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(45) CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(45) CHARACTER SET latin1 NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `email` varchar(45) CHARACTER SET latin1 NOT NULL,
  `fax` varchar(15) CHARACTER SET latin1 NOT NULL,
  `latitud` varchar(100) DEFAULT NULL,
  `longitud` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tienda`
--

INSERT INTO `tienda` (`id`, `nombre`, `direccion`, `ciudad`, `codigo_postal`, `telefono`, `email`, `fax`, `latitud`, `longitud`) VALUES
(1, 'Urzaiz', 'Calle Urzaiz num 3', 'Vigo', 36202, '55555555', 'urzais@gmail.com', '55555551', '42.2358184', '-8.719340500000044'),
(2, 'Camelias', 'Av Camelias num 5', 'Vigo', 36021, '66666666', 'camelias@gmail.com', '66666661', '42.2331657', '-8.728873799999974'),
(3, 'Arenal', 'Calle Arenal num 5', 'Vigo', 36203, '88888888', 'arenal@gmail.com', '88888881', '42.2402459', '-8.720811700000013');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `tipo`) VALUES
(1, 'administrador'),
(2, 'gestor'),
(3, 'registrado');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `NIF` varchar(9) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) NOT NULL,
  `telefono` varchar(15) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `localidad` varchar(45) NOT NULL,
  `provincia` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `fecha_sesion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `NIF`, `nombre`, `apellido1`, `apellido2`, `telefono`, `email`, `direccion`, `localidad`, `provincia`, `password`, `tipo`, `fecha_sesion`) VALUES
(15, '5647474D', 'prodigy', 'prodigy', 'progigy', '6545454', 'prodigy@abc.com', 'av calle 5', 'Madrid', 'Madrid', '$2y$10$cW.DHx0QyoJmbVlGogOj2u8wgzog4CUO2I9tpeoMJ7KIKsRL8txb2', 1, '2018-05-30 00:23:31'),
(17, '44446T', 'Administrador', 'Admi', 'Admi', '656578', 'administrador@ong.com', 'Av Vigo n 2', 'Pontevedra', 'Pontevedra', '$2y$10$kp6qCj858dTG.0ktCKxOSeoOLg5Q1HPmp4mzF.R/gBWtT5cXzfixe', 1, '2018-05-30 10:18:16'),
(18, '657890D', 'Gestor', 'Gest', 'Gest', '8756565', 'gestor@ong.com', 'Av Galicia n 7', 'Santiago', 'Coruña', '$2y$10$Oopx5oiydV9EfTJerW1I7eY2yODP.Oz/I/ivcdNTRTTuZi5XcLhw.', 2, '2018-05-30 09:50:58'),
(19, '657888P', 'Cliente', 'Cliente', 'Cliente', '6575757', 'cliente@ong.com', 'Av Rosalía', 'Vigo', 'Pontevedra', '$2y$10$bZceuOS/BZ/FYVTxTPqgt.bVRGMx5wX3TibM6BzsvYHB4F3L.eEVy', 3, '2018-05-30 08:36:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evento_lugar`
--
ALTER TABLE `evento_lugar`
  ADD PRIMARY KEY (`id_evento_lugar`),
  ADD KEY `id_evento` (`id_evento`),
  ADD KEY `id_lugar` (`id_lugar`);

--
-- Indexes for table `evento_participante`
--
ALTER TABLE `evento_participante`
  ADD PRIMARY KEY (`id_evento`,`id_participante`),
  ADD KEY `id_participante` (`id_participante`);

--
-- Indexes for table `fechas_posibles`
--
ALTER TABLE `fechas_posibles`
  ADD PRIMARY KEY (`id_evento`,`fecha`),
  ADD KEY `id_evento` (`id_evento`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CIF` (`CIF`);

--
-- Indexes for table `imagenes_prod`
--
ALTER TABLE `imagenes_prod`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ruta` (`ruta`),
  ADD KEY `id_prod` (`id_prod`);

--
-- Indexes for table `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lugar` (`lugar`);

--
-- Indexes for table `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupo` (`grupo`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`);

--
-- Indexes for table `producto_tienda`
--
ALTER TABLE `producto_tienda`
  ADD PRIMARY KEY (`id_producto_tienda`),
  ADD UNIQUE KEY `id_prod_2` (`id_prod`,`id_tienda`,`estado`),
  ADD KEY `id_prod` (`id_prod`),
  ADD KEY `id_tienda` (`id_tienda`),
  ADD KEY `estado` (`estado`);

--
-- Indexes for table `reserva_event`
--
ALTER TABLE `reserva_event`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_evento` (`id_evento`),
  ADD KEY `id_usuario_2` (`id_usuario`,`id_evento`);

--
-- Indexes for table `reserva_prod`
--
ALTER TABLE `reserva_prod`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `id_tienda` (`id_tienda`),
  ADD KEY `id_usuario_2` (`id_usuario`,`id_producto`,`id_tienda`);

--
-- Indexes for table `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `tipo` (`tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `evento_lugar`
--
ALTER TABLE `evento_lugar`
  MODIFY `id_evento_lugar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `imagenes_prod`
--
ALTER TABLE `imagenes_prod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `lugar`
--
ALTER TABLE `lugar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `participante`
--
ALTER TABLE `participante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `producto_tienda`
--
ALTER TABLE `producto_tienda`
  MODIFY `id_producto_tienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `reserva_event`
--
ALTER TABLE `reserva_event`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `reserva_prod`
--
ALTER TABLE `reserva_prod`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `evento_lugar`
--
ALTER TABLE `evento_lugar`
  ADD CONSTRAINT `evento_lugar_ibfk_1` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evento_lugar_ibfk_2` FOREIGN KEY (`id_lugar`) REFERENCES `lugar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evento_participante`
--
ALTER TABLE `evento_participante`
  ADD CONSTRAINT `evento_participante_ibfk_1` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento_lugar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evento_participante_ibfk_2` FOREIGN KEY (`id_participante`) REFERENCES `participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fechas_posibles`
--
ALTER TABLE `fechas_posibles`
  ADD CONSTRAINT `fk_evento` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento_lugar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `imagenes_prod`
--
ALTER TABLE `imagenes_prod`
  ADD CONSTRAINT `imagenes_prod_ibfk_1` FOREIGN KEY (`id_prod`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `participante`
--
ALTER TABLE `participante`
  ADD CONSTRAINT `participante_ibfk_1` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `producto_tienda`
--
ALTER TABLE `producto_tienda`
  ADD CONSTRAINT `producto_tienda_ibfk_1` FOREIGN KEY (`id_prod`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_tienda_ibfk_2` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_tienda_ibfk_3` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `reserva_event`
--
ALTER TABLE `reserva_event`
  ADD CONSTRAINT `reserva_event_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reserva_event_ibfk_2` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento_lugar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reserva_prod`
--
ALTER TABLE `reserva_prod`
  ADD CONSTRAINT `reserva_prod_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto_tienda` (`id_prod`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reserva_prod_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reserva_prod_ibfk_4` FOREIGN KEY (`id_tienda`) REFERENCES `producto_tienda` (`id_tienda`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`id`) ON UPDATE CASCADE;
--
-- Database: `ong1`
--
CREATE DATABASE IF NOT EXISTS `ong1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ong1`;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ruta_imagen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `evento_lugar`
--

CREATE TABLE `evento_lugar` (
  `id_evento_lugar` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `id_lugar` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `aforo` int(11) NOT NULL,
  `precio_entrada` float NOT NULL,
  `entradas_disponibles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `evento_participante`
--

CREATE TABLE `evento_participante` (
  `id_evento` int(11) NOT NULL,
  `id_participante` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fechas_posibles`
--

CREATE TABLE `fechas_posibles` (
  `id_evento` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `CIF` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `domicilio_fiscal` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `web` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `imagenes_prod`
--

CREATE TABLE `imagenes_prod` (
  `id` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `ruta` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lugar`
--

CREATE TABLE `lugar` (
  `id` int(11) NOT NULL,
  `lugar` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `participante`
--

CREATE TABLE `participante` (
  `id` int(11) NOT NULL,
  `NIF` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido1` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido2` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `localidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `provincia` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `grupo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `categoria` int(11) NOT NULL,
  `fecha_fin_campania` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `producto_tienda`
--

CREATE TABLE `producto_tienda` (
  `id_producto_tienda` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `id_tienda` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` float NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reserva_event`
--

CREATE TABLE `reserva_event` (
  `id_reserva` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reserva_prod`
--

CREATE TABLE `reserva_prod` (
  `id_reserva` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tienda`
--

CREATE TABLE `tienda` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(45) CHARACTER SET utf32 COLLATE utf32_spanish_ci NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fax` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `NIF` varchar(9) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido1` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `apellido2` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` int(15) NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `localidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `provincia` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `fecha_sesion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evento_lugar`
--
ALTER TABLE `evento_lugar`
  ADD PRIMARY KEY (`id_evento_lugar`),
  ADD KEY `eventoid` (`id_evento`),
  ADD KEY `lugarid` (`id_lugar`);

--
-- Indexes for table `evento_participante`
--
ALTER TABLE `evento_participante`
  ADD PRIMARY KEY (`id_evento`,`id_participante`),
  ADD KEY `participantekey` (`id_participante`);

--
-- Indexes for table `fechas_posibles`
--
ALTER TABLE `fechas_posibles`
  ADD PRIMARY KEY (`id_evento`,`fecha`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagenes_prod`
--
ALTER TABLE `imagenes_prod`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productoimg` (`id_prod`);

--
-- Indexes for table `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupoid` (`grupo`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoprodu` (`categoria`);

--
-- Indexes for table `producto_tienda`
--
ALTER TABLE `producto_tienda`
  ADD PRIMARY KEY (`id_producto_tienda`),
  ADD KEY `productoid` (`id_prod`),
  ADD KEY `tiendaid` (`id_tienda`),
  ADD KEY `estadoid` (`estado`);

--
-- Indexes for table `reserva_event`
--
ALTER TABLE `reserva_event`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `idusuario` (`id_usuario`),
  ADD KEY `idevent` (`id_evento`);

--
-- Indexes for table `reserva_prod`
--
ALTER TABLE `reserva_prod`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `usuarioid` (`id_usuario`),
  ADD KEY `productotienda` (`id_producto`);

--
-- Indexes for table `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipouser` (`tipo`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `evento_lugar`
--
ALTER TABLE `evento_lugar`
  ADD CONSTRAINT `eventoid` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lugarid` FOREIGN KEY (`id_lugar`) REFERENCES `lugar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evento_participante`
--
ALTER TABLE `evento_participante`
  ADD CONSTRAINT `eventokey` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento_lugar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participantekey` FOREIGN KEY (`id_participante`) REFERENCES `participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fechas_posibles`
--
ALTER TABLE `fechas_posibles`
  ADD CONSTRAINT `idevento` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `imagenes_prod`
--
ALTER TABLE `imagenes_prod`
  ADD CONSTRAINT `productoimg` FOREIGN KEY (`id_prod`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `participante`
--
ALTER TABLE `participante`
  ADD CONSTRAINT `grupoid` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `categoprodu` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `producto_tienda`
--
ALTER TABLE `producto_tienda`
  ADD CONSTRAINT `estadoid` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productoid` FOREIGN KEY (`id_prod`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tiendaid` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reserva_event`
--
ALTER TABLE `reserva_event`
  ADD CONSTRAINT `idevent` FOREIGN KEY (`id_evento`) REFERENCES `evento_lugar` (`id_evento_lugar`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idusuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reserva_prod`
--
ALTER TABLE `reserva_prod`
  ADD CONSTRAINT `productotienda` FOREIGN KEY (`id_producto`) REFERENCES `producto_tienda` (`id_producto_tienda`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarioid` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `tipouser` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

--
-- Dumping data for table `pma__designer_settings`
--

INSERT INTO `pma__designer_settings` (`username`, `settings_data`) VALUES
('phpmyadmin', '{"angular_direct":"direct","snap_to_grid":"off","relation_lines":"true"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

--
-- Dumping data for table `pma__export_templates`
--

INSERT INTO `pma__export_templates` (`id`, `username`, `export_type`, `template_name`, `template_data`) VALUES
(1, 'phpmyadmin', 'database', 'fxgqtdwy_ong', '{"quick_or_custom":"quick","what":"sql","structure_or_data_forced":"0","table_select[]":["categoria","estado","evento","evento_lugar","evento_participante","fechas_posibles","grupo","imagenes_prod","lugar","participante","producto","producto_tienda","reserva_event","reserva_prod","tienda","tipo_usuario","usuario"],"table_structure[]":["categoria","estado","evento","evento_lugar","evento_participante","fechas_posibles","grupo","imagenes_prod","lugar","participante","producto","producto_tienda","reserva_event","reserva_prod","tienda","tipo_usuario","usuario"],"table_data[]":["categoria","estado","evento","evento_lugar","evento_participante","fechas_posibles","grupo","imagenes_prod","lugar","participante","producto","producto_tienda","reserva_event","reserva_prod","tienda","tipo_usuario","usuario"],"output_format":"sendit","filename_template":"@DATABASE@","remember_template":"on","charset":"utf-8","compression":"none","maxsize":"","pdf_report_title":"","pdf_structure_or_data":"structure_and_data","htmlword_structure_or_data":"structure_and_data","htmlword_null":"NULL","excel_null":"NULL","excel_edition":"win","excel_structure_or_data":"data","sql_include_comments":"something","sql_header_comment":"","sql_compatibility":"NONE","sql_structure_or_data":"structure_and_data","sql_create_table":"something","sql_auto_increment":"something","sql_create_view":"something","sql_procedure_function":"something","sql_create_trigger":"something","sql_backquotes":"something","sql_type":"INSERT","sql_insert_syntax":"both","sql_max_query_size":"50000","sql_hex_for_binary":"something","sql_utc_time":"something","csv_separator":",","csv_enclosed":"\\"","csv_escaped":"\\"","csv_terminated":"AUTO","csv_null":"NULL","csv_structure_or_data":"data","ods_null":"NULL","ods_structure_or_data":"data","texytext_structure_or_data":"structure_and_data","texytext_null":"NULL","phparray_structure_or_data":"data","json_structure_or_data":"data","xml_structure_or_data":"data","xml_export_events":"something","xml_export_functions":"something","xml_export_procedures":"something","xml_export_tables":"something","xml_export_triggers":"something","xml_export_views":"something","xml_export_contents":"something","yaml_structure_or_data":"data","odt_structure_or_data":"structure_and_data","odt_relation":"something","odt_comments":"something","odt_mime":"something","odt_columns":"something","odt_null":"NULL","latex_caption":"something","latex_structure_or_data":"structure_and_data","latex_structure_caption":"Structure of table @TABLE@","latex_structure_continued_caption":"Structure of table @TABLE@ (continued)","latex_structure_label":"tab:@TABLE@-structure","latex_relation":"something","latex_comments":"something","latex_mime":"something","latex_columns":"something","latex_data_caption":"Content of table @TABLE@","latex_data_continued_caption":"Content of table @TABLE@ (continued)","latex_data_label":"tab:@TABLE@-data","latex_null":"\\\\textit{NULL}","codegen_structure_or_data":"data","codegen_format":"0","mediawiki_structure_or_data":"structure_and_data","mediawiki_caption":"something","mediawiki_headers":"something","":null,"lock_tables":null,"as_separate_files":null,"htmlword_columns":null,"excel_removeCRLF":null,"excel_columns":null,"sql_dates":null,"sql_relation":null,"sql_mime":null,"sql_use_transaction":null,"sql_disable_fk":null,"sql_views_as_tables":null,"sql_metadata":null,"sql_create_database":null,"sql_drop_table":null,"sql_if_not_exists":null,"sql_truncate":null,"sql_delayed":null,"sql_ignore":null,"csv_removeCRLF":null,"csv_columns":null,"ods_columns":null,"texytext_columns":null,"json_pretty_print":null}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('phpmyadmin', '[{"db":"ong","table":"producto"},{"db":"ong","table":"producto_tienda"},{"db":"ong","table":"estado"},{"db":"ong","table":"evento"},{"db":"ong","table":"evento_lugar"},{"db":"ong","table":"tipo_usuario"},{"db":"ong","table":"usuario"},{"db":"mysql","table":"user"},{"db":"ong","table":"participante"},{"db":"ong","table":"grupo"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('phpmyadmin', 'ong', 'categoria', '{"sorted_col":"`categoria`.`id` ASC"}', '2018-05-27 12:06:48'),
('phpmyadmin', 'ong', 'estado', '{"sorted_col":"`estado`.`id` ASC"}', '2018-05-27 11:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('phpmyadmin', '2018-05-29 23:43:50', '{"collation_connection":"utf8mb4_unicode_ci"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `receitas`
--
CREATE DATABASE IF NOT EXISTS `receitas` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `receitas`;

-- --------------------------------------------------------

--
-- Table structure for table `chef`
--

CREATE TABLE `chef` (
  `codigo` smallint(6) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `apelido1` varchar(30) NOT NULL,
  `apelido2` varchar(30) DEFAULT NULL,
  `nomeartistico` varchar(40) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `data_nacemento` date DEFAULT NULL,
  `localidade` varchar(50) DEFAULT NULL,
  `cod_provincia` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chef`
--

INSERT INTO `chef` (`codigo`, `nome`, `apelido1`, `apelido2`, `nomeartistico`, `sexo`, `data_nacemento`, `localidade`, `cod_provincia`) VALUES
(1, 'XULIAN', 'CONDE', 'FLORES', 'XULIANICO', 'H', '1969-05-06', 'NIGRÁN', '36'),
(2, 'MARIA', 'MAGADÁN', 'PÉREZ', 'MAMA MAGA', 'M', '1973-12-06', 'MONFORTE', '27'),
(3, 'FELIPE', 'ARIAS', 'CONDE', 'FELIPIN', 'H', '1964-01-01', 'PONTEVEDRA', '36'),
(4, 'XOEL', 'GARCÍA', 'BARREIRO', 'EL TITO', 'H', '1973-11-09', 'LUGO', '27'),
(5, 'MIRIAM', 'LÓPEZ', 'SIERRA', 'MIRINDA', 'M', '1959-04-04', 'SANTIAGO', '15'),
(6, 'BELÉN', 'REDONDO', 'RUIZ', 'BELBELÉN', 'M', '1965-03-02', 'SANTIAGO', '15'),
(7, 'LUCIA', 'TARREGA', 'VARELA', 'LUCITA', 'M', '1967-10-12', 'ALCOBENDAS', '28'),
(8, 'MARISA', 'GIL', 'PEREIRA', 'PERITA EN DULCE', 'M', '1973-02-11', 'A ESTRADA', '36'),
(9, 'CARLOS', 'DIEGUEZ', 'RODRÍGUEZ', 'CARLIÑOS', 'H', '1969-08-05', 'NEGREIRA', '15'),
(10, 'ELISEO', 'SEOANE', 'SEOANE', 'SEOANE', 'H', '1971-02-03', 'LUGO', '27'),
(11, 'CHEMA', 'VARELA', 'RODRIGUEZ', 'CHEMITA', 'H', '1971-01-12', 'PONTEVEDRA', '36');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `codigo` tinyint(4) NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`codigo`, `nome`) VALUES
(1, 'ENTRANTES'),
(2, 'TAPAS'),
(3, 'ARROCES'),
(4, 'PASTAS'),
(5, 'VERDURAS'),
(6, 'CARNES'),
(7, 'PESCADOS'),
(8, 'MARISCOS'),
(9, 'SOBREMESAS');

-- --------------------------------------------------------

--
-- Table structure for table `ingrediente`
--

CREATE TABLE `ingrediente` (
  `codigo` smallint(6) NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ingrediente`
--

INSERT INTO `ingrediente` (`codigo`, `nome`) VALUES
(1, 'CHICHARO'),
(2, 'XUDIAS'),
(3, 'CENORIA'),
(4, 'BRÓCOLI'),
(5, 'PATACAS'),
(6, 'SAL'),
(7, 'AZUCAR'),
(8, 'PEMENTA'),
(9, 'COMIÑO'),
(10, 'AMEIXAS'),
(11, 'MEXILÓNS'),
(12, 'ARROZ'),
(13, 'TALLARÍNS'),
(14, 'MACARRÓNS'),
(15, 'OVOS'),
(16, 'NATA'),
(17, 'COGOMELOS'),
(18, 'PEMENTOS'),
(19, 'ALLO PORRO'),
(20, 'CEBOLA'),
(21, 'ALLO'),
(22, 'LEITE'),
(23, 'QUEIXO'),
(24, 'GARAVANZOS'),
(25, 'SOLOMBO'),
(26, 'FARIÑA'),
(27, 'MANTEIGA'),
(28, 'LÉVEDO'),
(29, 'LEITE CONDENSADO'),
(30, 'AMORODO'),
(31, 'LEMÓN'),
(32, 'MARMELO'),
(33, 'ACEITE'),
(34, 'ROBALIZA'),
(35, 'CURRY'),
(36, 'MANZÁS'),
(37, 'ANANÁS'),
(38, 'ESPINACAS');

-- --------------------------------------------------------

--
-- Table structure for table `provincia`
--

CREATE TABLE `provincia` (
  `codigo` char(2) NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provincia`
--

INSERT INTO `provincia` (`codigo`, `nome`) VALUES
('15', 'A Coruña'),
('24', 'León'),
('27', 'Lugo'),
('28', 'Madrid'),
('32', 'Ourense'),
('36', 'Pontevedra');

-- --------------------------------------------------------

--
-- Table structure for table `receita`
--

CREATE TABLE `receita` (
  `codigo` smallint(6) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `cod_grupo` tinyint(4) NOT NULL,
  `dificultade` varchar(7) NOT NULL,
  `tempo` tinyint(4) DEFAULT NULL,
  `elaboracion` varchar(300) DEFAULT NULL,
  `cod_chef` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `receita`
--

INSERT INTO `receita` (`codigo`, `nome`, `cod_grupo`, `dificultade`, `tempo`, `elaboracion`, `cod_chef`) VALUES
(1, 'ARROZ AO COMIÑO', 3, 'Media', 50, 'Rehógase en aceite o allo porro con ....', 11),
(2, 'PEMENTOS RECHEOS', 5, 'Difícil', 80, 'Ásanse os pementos ....', 11),
(3, 'PELUXE BRANCA', 2, 'Difícil', 45, 'Cocénse os garavanzos en ....', 8),
(4, 'TALLARÍNS VERDES', 4, 'Doada', 25, 'Cócense os tallaríns en auga salgada e ...', 6),
(5, 'PASTA CON SETAS', 4, 'Media', 45, 'Ponse a ...', 6),
(6, 'ROBALIZA Á SAL', 7, 'Doada', 30, 'Precalentar o forno a ...', 3),
(7, 'AMORODOS Á PEMENTA DE XAMAICA', 9, 'Doada', 15, 'Lavar os amorodos e', 11),
(8, 'TORTA DE MANZÁ', 9, 'Media', 60, 'Mezclar a fariña ..', 4),
(9, 'QUEIXO CON ANANÁS', 1, 'Doada', 15, 'Córtase a ..', 7),
(10, 'AMEIXAS VERDES', 9, 'Difícil', 90, 'Cócense as ...', 1),
(11, 'MEXILÓNS RECHEOS', 9, 'Difícil', 80, 'Cócense as ...', 3),
(12, 'OVOS DOCES', 1, 'Media', 30, 'Cócense os ...', 5),
(13, 'AMORODOS CON NATA', 9, 'Doada', 20, 'Bater a ...', 9),
(14, 'SOLOMBO Á PEMENTA', 6, 'Doada', 20, 'Se ...', 10),
(15, 'SOLOMBO Á MIA MAMA', 6, 'Media', 25, 'Se ...', 2),
(16, 'PATACAS AO POBRE', 5, 'Media', 30, 'Se ...', 2);

-- --------------------------------------------------------

--
-- Table structure for table `receita_ingrediente`
--

CREATE TABLE `receita_ingrediente` (
  `cod_receita` smallint(6) NOT NULL,
  `cod_ingrediente` smallint(6) NOT NULL,
  `cantidade` smallint(6) DEFAULT NULL,
  `medida` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `receita_ingrediente`
--

INSERT INTO `receita_ingrediente` (`cod_receita`, `cod_ingrediente`, `cantidade`, `medida`) VALUES
(1, 9, 1, 'culleradas'),
(1, 12, 300, 'gramos'),
(1, 16, 0, 'litros'),
(1, 18, 1, 'unidade'),
(1, 33, 5, 'culleradas'),
(2, 4, 300, 'gramos'),
(2, 18, 4, 'unidade'),
(2, 19, 1, 'unidade'),
(2, 23, 100, 'gramos'),
(2, 33, 10, 'culleradas'),
(3, 5, 2, 'culleradas'),
(3, 16, 0, 'litros'),
(3, 19, 1, 'unidade'),
(3, 24, 200, 'gramos'),
(4, 1, 100, 'gramos'),
(4, 2, 50, 'gramos'),
(4, 6, 1, 'pizca'),
(4, 13, 250, 'gramos'),
(5, 5, 1, 'pizca'),
(5, 14, 200, 'gramos'),
(5, 17, 250, 'gramos'),
(6, 6, 1000, 'gramos'),
(6, 34, 1, 'unidade'),
(7, 8, 1, 'culleradas'),
(7, 30, 500, 'gramos'),
(7, 31, 1, 'unidade'),
(8, 7, 8, 'culleradas'),
(8, 26, 10, 'culleradas'),
(8, 28, 1, 'culleradas'),
(8, 32, 4, 'culleradas'),
(9, 23, 250, 'gramos'),
(9, 37, 1, 'unidade'),
(10, 4, 150, 'gramos'),
(10, 6, 1, 'pizca'),
(10, 10, 1000, 'gramos'),
(10, 27, 50, 'gramos'),
(11, 6, 1, 'pizca'),
(11, 11, 1000, 'gramos'),
(11, 15, 2, 'unidade'),
(11, 23, 50, 'gramos'),
(12, 7, 5, 'culleradas'),
(12, 15, 5, 'gramos'),
(12, 16, 0, 'litros'),
(13, 7, 2, 'culleradas'),
(13, 16, 0, 'litros'),
(13, 30, 500, 'gramos'),
(14, 8, 1, 'culleradas'),
(14, 25, 1000, 'gramos'),
(14, 33, 3, 'culleradas'),
(15, 16, 0, 'litros'),
(15, 25, 1000, 'gramos'),
(15, 33, 3, 'culleradas'),
(16, 5, 5, 'unidade'),
(16, 6, 1, 'pizca'),
(16, 33, 6, 'culleradas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chef`
--
ALTER TABLE `chef`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_chef_provincia` (`cod_provincia`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `receita`
--
ALTER TABLE `receita`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_receita_chef` (`cod_chef`);

--
-- Indexes for table `receita_ingrediente`
--
ALTER TABLE `receita_ingrediente`
  ADD PRIMARY KEY (`cod_receita`,`cod_ingrediente`),
  ADD KEY `fk_receita_ingrediente` (`cod_ingrediente`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chef`
--
ALTER TABLE `chef`
  ADD CONSTRAINT `fk_chef_provincia` FOREIGN KEY (`cod_provincia`) REFERENCES `provincia` (`codigo`);

--
-- Constraints for table `receita`
--
ALTER TABLE `receita`
  ADD CONSTRAINT `fk_receita_chef` FOREIGN KEY (`cod_chef`) REFERENCES `chef` (`codigo`);

--
-- Constraints for table `receita_ingrediente`
--
ALTER TABLE `receita_ingrediente`
  ADD CONSTRAINT `fk_receita_ingrediente` FOREIGN KEY (`cod_ingrediente`) REFERENCES `ingrediente` (`codigo`),
  ADD CONSTRAINT `fk_receita_receita` FOREIGN KEY (`cod_receita`) REFERENCES `receita` (`codigo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

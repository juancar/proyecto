Información del proyecto: Proyecto desarrollado durante el tiempo de formación profesional. El proyecto consistía en una página web que permitiera la reserva de productos y eventos a los usuarios clientes, así  como la gestión de los productos, eventos y tiendas por parte de los miembros de una ONG. En el proyecto se desarrollo tanto el front-end como el back-end. Para poder realizar las operaciones de reserva por parte de los clientes y de añadir, modificar o quitar productos, eventos y tiendas por parte de los miembros de la ONG es necesario estar registrado e iniciar sesión.

Tecnologías utilizadas: Javascript, AngularJS, JQuery, PHP, Apache, MySQL.

Para ver el proyecto acceder a: https://www.rayadita.es/proyecto

Más información del proyecto: https://www.rayadita.es/datus.html